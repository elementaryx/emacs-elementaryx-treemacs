;;; ElementaryX: Elementary Emacs configuration coupled with Guix
;;;
;;; Extra config: Treemacs enhancement

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Treemacs: a tree layout file explorer for Emacs
;;;   (typical tree sidebar of an IDE)
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://github.com/Alexander-Miller/treemacs
;; (use-package treemacs
;;   :bind
;;   (:map global-map
;;         ;; ("M-0"       . treemacs-select-window)
;;         ;; ("C-x t 1"   . treemacs-delete-other-windows)
;; 	("C-x 1"   . treemacs-delete-other-windows)
;;         ("C-c t t"   . treemacs)
;;         ;; ("C-x t d"   . treemacs-select-directory)
;;         ;; ("C-x t B"   . treemacs-bookmark)
;;         ;; ("C-x t C-t" . treemacs-find-file)
;;         ;; ("C-x t M-t" . treemacs-find-tag))
;; 	))

;; https://github.com/Alexander-Miller/treemacs
(use-package treemacs
  :bind
  (
        ;; ("M-0"       . treemacs-select-window)
        ;; ("C-x t 1"   . treemacs-delete-other-windows)
	("C-x 1"   . treemacs-delete-other-windows)
        ("C-c t t"   . treemacs)
        ;; ("C-x t d"   . treemacs-select-directory)
        ;; ("C-x t B"   . treemacs-bookmark)
        ;; ("C-x t C-t" . treemacs-find-file)
        ;; ("C-x t M-t" . treemacs-find-tag))
	))

(use-package treemacs-magit
  :after (treemacs magit))

(use-package treemacs-evil
  :after (treemacs evil)) ;; enabled only when evil enabled

;; https://github.com/Alexander-Miller/treemacs/tree/master#treemacs-tab-bar :
;; Integration with tab-bar-mode that allows treemacs buffers to be
;; unique inside the active tab instead of the default frame-based
;; buffer scope.
;; See also emacs reference: https://www.gnu.org/software/emacs/manual/html_node/emacs/Tab-Bars.html
(use-package treemacs-tab-bar ;;treemacs-tab-bar if you use tab-bar-mode
  :after (treemacs)
  :config (treemacs-set-scope-type 'Tabs))

(provide 'elementaryx-treemacs)
